<?php
include('config/bdd.php');
session_start();
if(!isset($_SESSION['login'])) {
	header('Location: /login.php');
	exit();
}

$sql = 'SELECT COUNT(*) FROM log_report';
$req = $bdd->prepare($sql);
$req->execute();

while($row = $req->fetch()) {
	$nb = $row['COUNT(*)'];
}

if ($nb > 20) {
	$nb = 20;
}

?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<?php include('php_include/head.html'); ?>
	<style>
	table {
		width:100%;
	}
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}
	th, td {
		padding: 15px;
		text-align: left;
	}
	table tr:nth-child(even) {
		background-color: #eee;
		color: black;
	}
	table tr:nth-child(odd) {
		background-color: #fff;
		color: black;
	}
	table th {
		background-color: black;
		color: white;
	}
	#table {

	}
</style>
</head>
<body>
	<?php include('php_include/menu.html'); ?>
	<div class="container">
		<header>
			<h1>Derniers reports:<span></span></h1>
			<div id="table">
			<table>
				<tr>
					<th>ID</th>
					<th>Joueur</th>
					<th>Report par</th>
					<th>Date</th>
					<th>Raison</th>
				</tr>
				<?php
				$sqll = 'SELECT * FROM (SELECT * FROM log_report ORDER BY id DESC LIMIT ' . $nb . ') sub ORDER BY id DESC';
				$reqq = $bdd->prepare($sqll);
				$reqq->execute();

				while ($roww = $reqq->fetch()) {
					echo "<tr>";
					echo "<td>" . $roww['id'] . "</td>";
					echo "<td>" . $roww['player'] . "</td>";
					echo "<td>" . $roww['moderator'] . "</td>";
					echo "<td>" . $roww['date'] . "</td>";
					echo "<td>" . $roww['raison'] . "</td>";
					echo "</tr>";
				}

				?>
			</table>
		</div>
		</header>
	</div><!-- /container -->
	<script src="js/classie.js"></script>
	<script src="js/gnmenu.js"></script>
	<script>
		new gnMenu( document.getElementById( 'gn-menu' ) );
	</script>
</body>
</html>
