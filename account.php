<?php
include('config/bdd.php');
session_start(); // Obligatoirement avant tout `echo`, `print` ou autre texte HTML.

if(!isset($_SESSION['login'])) {
	header('Location: connect');
	exit();	
}
?>

<!DOCTYPE html>
<html lang="fr" class="no-js">
<head>
	<?php include('php_include/head.html'); ?>
</head>
<body>
	<?php include('php_include/menu.html'); ?>
	<div class="container">
		<header>
			<h1>Mon compte :</h1>	
				<label>Nom d'utilisateur : <?php echo $_SESSION['pseudo']; ?></label>
				<br>
				<label>Type de compte : <?php if($_SESSION['type'] == 0) {echo "Helper\n"; } else if ($_SESSION['type'] == 1) { echo "Modérateur\n"; } else if ($_SESSION['type'] == 2) { echo "Administrateur\n"; } ?></label>
				<br>
			</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
	</html>