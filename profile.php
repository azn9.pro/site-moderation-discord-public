<?php
	include('config/bdd.php');
	session_start();

	$email = $_GET['user'];

	$sql = 'SELECT COUNT(*) FROM users WHERE email = ?';
	$req = $bdd->prepare($sql);
	$req->execute(array($email));
	while($row = $req->fetchColumn()) {
		$nb = $row;
	}
	
	if(isset($nb)) {
		$sql2 = 'SELECT * FROM users WHERE email = ?';
		$req2 = $bdd->prepare($sql2);
		$req2->execute(array($email));

		while($row2 = $req2->fetch()) {
			$type = $row2['type'];

			$description = $row2['description'];
			$description = htmlentities($description);
			if ($type == 0) {
				header("404");
				exit();
			}
		}
	} else {
		header("Location: 404");
		exit();
	}
?>
<!DOCTYPE html>
<html lang="fr" class="no-js">
	<head>
		<?php include('php_include/head.html'); ?>
	</head>
	<body>
		<?php include('php_include/menu.html'); ?>
		<div class="container">
			<header>
				<h1><?php echo $email, "<span>", $description, "</span></h1>";

				

				?>
				</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
</html>