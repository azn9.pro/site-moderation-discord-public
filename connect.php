<?php
include('config/bdd.php');
session_start(); // Obligatoirement avant tout `echo`, `print` ou autre texte HTML.
if(isset($_SESSION['login'])) {
    header('Location: account');
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr" class="no-js">
	<head>
		<?php include('php_include/head.html'); ?>
	</head>
	<body>
		<?php include('php_include/menu.html'); ?>
		<div class="container">
			<header>
				<h1>Rejoindre MuseLive :</h1>	
			
			<a href="login"><button>Se connecter</button></a>
			<a href="register"><button>S'inscrire</button></a>
				</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
</html>